package com.company.View;

import com.company.model.User;
import com.company.repository.BookRepository;
import com.company.repository.BorrowRepository;
import com.company.repository.UserRepository;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Scanner;

import static com.company.repository.RepositoryDAO.bookRepository;
import static com.company.repository.RepositoryDAO.borrowRepository;

public class BorrowProcess {

    public void setborrow(int bookId, int userId, Timestamp start, Timestamp end) throws SQLException {

        BorrowRepository borrowRepository = new BorrowRepository();
        BookRepository bookRepository = new BookRepository();
        borrowRepository.creatBorrow(userId, bookId, start, end);
        for (int i = 0; i < bookRepository.readBook().size(); i++) {
            if (bookRepository.readBook().get(i).isStatus() == 1) {
                System.out.println("ketab mojod nist");
            } else {
                bookRepository.updateBook(bookId, 1);
            }
        }

    }

    public void returnBook(Timestamp returnDate) throws SQLException {
        BookRepository bookRepository = new BookRepository();

        for (int i = 0; i < bookRepository.readBook().size(); i++) {
            if (bookRepository.readBook().get(i).isStatus() == 1) {
                System.out.println(i+1+")" + " " +bookRepository.readBook().get(i).getTitle());
                Scanner scanner = new Scanner(System.in);
                int bookNumber = scanner.nextInt();
                BorrowRepository borrowRepository = new BorrowRepository();
                System.out.println(borrowRepository.readBorrow().get(i));
                bookRepository.updateBook(bookNumber,0);
                System.out.println(bookRepository.readBook().get(i).getPenalty());
                UserRepository userRepository = new UserRepository();

                if(returnDate.after(borrowRepository.readBorrow().get(i).getEnd())){
                  int lastBodget = (int) (userRepository.readUser().getBudget() - bookRepository.readBook().get(i).getPenalty());
                   userRepository.updateUser(lastBodget,i+1);
                }

                System.out.println(userRepository.readUser().getBudget());
            }
        }
    }
}
