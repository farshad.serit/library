package com.company.View;

import com.company.model.Book;
import com.company.repository.BookRepository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class ShowMenu {


    public void getBookFromDb() throws SQLException {
        BookRepository bookRepository = new BookRepository();

        System.out.println("select your book(number):");
        for (int i = 0; i <bookRepository.readBook().size() ; i++) {
            System.out.println(i+1+")" + " " + bookRepository.readBook().get(i).getTitle());
        }

    }
}
