package com.company.controller;

import com.company.View.BorrowProcess;
import com.company.View.ShowLoginPage;
import com.company.View.ShowMenu;
import com.company.model.Borrow;
import com.company.model.User;
import com.company.repository.UserRepository;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Scanner;

public class Main {



    public static void main(String[] args) throws SQLException {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        Timestamp now = Timestamp.valueOf(LocalDateTime.now());
        Timestamp end = Timestamp.valueOf(LocalDateTime.now().plusDays(5));

        ShowLoginPage loginPage = new ShowLoginPage();
        UserRepository userRepository = new UserRepository();
        User  user = userRepository.readUser();
        Scanner scanner = new Scanner(System.in);
        BorrowProcess borrowProcess = new BorrowProcess();

            try {
                if (loginPage.login()) {
                    for (int i = 0; i < i + 1; i++) {
                        System.out.println("1)" + " " + "Borrowing  book:");
                        System.out.println("2)" + " " + "return book:");
                        int menu = scanner.nextInt();
                        if (menu == 1) {
                            ShowMenu showMenu = new ShowMenu();
                            showMenu.getBookFromDb();
                            int bookNumber = scanner.nextInt();
                            borrowProcess.setborrow(bookNumber, user.getUserId(),
                                   now, end);
                        } else if (menu == 2) {
                            borrowProcess.returnBook(end);
                        }

                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
    }

}
