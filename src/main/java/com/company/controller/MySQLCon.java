package com.company.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class MySQLCon {
    private Connection msCon = null;
    public static String ERROR = null;

    private MySQLCon(String database) {
        registerDriver(database);
    }

    private void registerDriver(String database){
        try{
            String url = "jdbc:mysql://localhost/";
            String driver = "com.mysql.jdbc.Driver";

            Class.forName(driver).newInstance();

            Properties property = new Properties();
            property.setProperty("user", "root");
            property.setProperty("password", "");
            // for farsi unicode
            property.setProperty("useUnicode", "yes");
            property.setProperty("characterEncoding", "UTF-8");

             msCon = DriverManager.getConnection(url, property);
             msCon.setCatalog(database);

        }catch(Exception e){
            ERROR = e.getMessage();

        }
    }

    public static MySQLCon getInstance(String database){
        return new MySQLCon(database);

    }

    public Connection getConnection(){
        return msCon;

    }
}
