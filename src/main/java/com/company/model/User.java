package com.company.model;

import java.util.Date;

public class User {

     int userId;
     String name  ;
     String password ;
     Date creat_at;
     int nationalCode;
     boolean status;
     long budget ;

    public User() {
    }

    public User(int userId, String name, String password, Date creat_at, int nationalCode, boolean status, long budget) {
        this.userId = userId;
        this.name = name;
        this.password = password;
        this.creat_at = creat_at;
        this.nationalCode = nationalCode;
        this.status = status;
        this.budget = budget;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreat_at() {
        return creat_at;
    }

    public void setCreat_at(java.sql.Date creat_at) {
        this.creat_at = creat_at;
    }

    public int getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(int nationalCode) {
        this.nationalCode = nationalCode;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public long getBudget() {
        return budget;
    }

    public void setBudget(long budget) {
        this.budget = budget;
    }
}
