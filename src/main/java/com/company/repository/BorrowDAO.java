package com.company.repository;

import com.company.controller.MySQLCon;
import com.company.model.Borrow;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

public interface BorrowDAO {

    MySQLCon obj = MySQLCon.getInstance("library");
    Connection msCon = obj.getConnection();

//user can get book from library and set start date
    public void creatBorrow(int userId, int bookId, Timestamp startBorrow, Timestamp endBorrow) throws SQLException;

//View user information and book borrowed
    public ArrayList<Borrow> readBorrow() throws SQLException;

//can change borrowed book  information
    public void updateBorrow(int bookId,int id) throws SQLException;

//delet borrowed book information
    public void deletBorrow();

    public Borrow searchById(int id) throws SQLException;

    public void findAll() throws SQLException;

}
