package com.company.repository;

import com.company.model.Borrow;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

public class BorrowRepository implements BorrowDAO{

    //===========================================================
    @Override
    public void creatBorrow(int userId, int bookId, Timestamp startBorrow, Timestamp endBorrow) throws SQLException {

        PreparedStatement statement  = msCon.prepareStatement(
                String.format("INSERT INTO borrows  ( user_id, book_id,start,end) VALUES  (?, ?, ?, ?);"));

        //  statement.setInt(1, id);
            statement.setInt(1, userId);
            statement.setInt(2, bookId);
            statement.setTimestamp(3, startBorrow);
            statement.setTimestamp(4,endBorrow);
            statement.executeUpdate();

     //===========================================================
    }
    @Override
    public ArrayList<Borrow> readBorrow() throws SQLException {
        PreparedStatement statement = msCon.prepareStatement("select * from borrows");
        ResultSet resultSet = statement.executeQuery();
        ArrayList<Borrow> borrows =new ArrayList<>();
        while (resultSet.next()){
            Borrow borrow = new Borrow();
            borrow.setBookId(resultSet.getInt("book_id"));
            borrow.setUserId(resultSet.getInt("user_id"));
            borrow.setStart(resultSet.getDate("start"));
            borrow.setEnd(resultSet.getDate("end"));

            borrows.add(borrow);
        }

  return borrows;
    }
    //===========================================================

    @Override
    public void updateBorrow(int bookId,int id) throws SQLException {
        PreparedStatement statement = msCon.prepareStatement("update borrows set book_id = ? where id = ?;");
        statement.setInt(1,bookId);
        statement.setInt(2,id);
        statement.executeUpdate();



    }
//===================================================================
    @Override
    public void deletBorrow() {
        //TODO complet this method

    }
//=========================================================================
    @Override
    public Borrow searchById(int id) throws SQLException {
        PreparedStatement statement = msCon.prepareStatement("select * from borrows where id = ?");
        statement.setInt(1,id);
        ResultSet resultSet = statement.executeQuery();
        Borrow borrow = new Borrow();
        if (resultSet.next()){
            borrow.setBookId(resultSet.getInt("book_id"));
            borrow.setUserId(resultSet.getInt("user_id"));
            borrow.setStart(resultSet.getDate("start"));
            borrow.setEnd(resultSet.getDate("end"));
        }
       return borrow;
    }
//===========================================================================
    @Override
    public void findAll() throws SQLException {

        PreparedStatement statement = msCon.prepareStatement("select * from borrows;");
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()){
            int test = resultSet.getInt("book_id");
            System.out.println(test);
        }

    }
}
