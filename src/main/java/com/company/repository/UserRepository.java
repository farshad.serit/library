package com.company.repository;

import com.company.model.User;

import java.sql.*;

public class UserRepository implements UserDAO {


//=============================================================================
    public void creatUser(String fullName,String creatAt,int nationCode,int status,int budget) throws SQLException {

      PreparedStatement Statement  = msCon.prepareStatement("INSERT INTO users" +
              "  ( full_name, creat_at, national_code, status,bodget) VALUES " +
              " (?, ?, ?, ?, ?);");

       //   Statement.setInt(1,id);
          Statement.setString(1,fullName);
          Statement.setString(2,creatAt);
          Statement.setInt(3,nationCode);
          Statement.setInt(4,status);
          Statement.setInt(5,budget);
          System.out.println(Statement);
          Statement.executeUpdate();



    }
//=================================================
    public User readUser() throws SQLException {

            PreparedStatement statement ;

            statement = msCon.prepareStatement("select * from users");
            ResultSet resultSet = statement.executeQuery();
             User uname = new User();
            while (resultSet.next()) {

                uname.setName(resultSet.getString("full_name"));
                uname.setPassword(resultSet.getString("password"));
                uname.setBudget(resultSet.getInt("bodget"));
                uname.setNationalCode(resultSet.getInt("national_code"));
                uname.setUserId(resultSet.getInt("id"));
            }
            return uname;
    }

//======================================================================
    public void updateUser(long bodget,int id) throws SQLException {

        PreparedStatement statement = msCon.prepareStatement("update users set bodget = ? where id = ?;");
      //  statement.setString(1,fullName);
        statement.setLong(1,bodget);
        statement.setInt(2,id);
        statement.executeUpdate();

    }
//======================================================================
    public void deletUser() {
// TODO: ۱۸/۰۱/۲۰۲۰  get id and delet user
    }
//===========================================================
    //get id and show data
    public User sarchById(int id) throws SQLException {

      PreparedStatement statement = msCon.prepareStatement("select * from users where id = ?;");
        statement.setInt(1,id);
        ResultSet resultSet = statement.executeQuery();
        User user =new User();
        if (resultSet.next()){
            user.setUserId(resultSet.getInt("id"));
            user.setName(resultSet.getString("full_name"));
            user.setCreat_at(resultSet.getDate("creat_at"));
            user.setNationalCode(resultSet.getInt("national_code"));
            user.setStatus(resultSet.getBoolean("status"));
            user.setBudget(resultSet.getInt("budget"));
        }
        return user;
    }
//================================================================
    public void findAll() throws SQLException {
        PreparedStatement statement ;

        statement = msCon.prepareStatement("select * from users");
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {
            String test = resultSet.getString("national_code");
            System.out.println(test);

        }

    }

}
