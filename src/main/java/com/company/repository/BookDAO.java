package com.company.repository;

import com.company.controller.MySQLCon;
import com.company.model.Book;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

public interface BookDAO {
    MySQLCon obj = MySQLCon.getInstance("library");
    Connection msCon = obj.getConnection();

// creat book means insert Specifications book into database
    public void creatBook(int id,String title,String subject,int isbn,int status,int price,int penalty) throws SQLException;

    //read book means get Specifications book from database
    public ArrayList<Book> readBook() throws SQLException;

    //update book means insert new data book into database
    public void updateBook( int id,int status) throws SQLException;

    //delet book means remove Specifications book from database
    public void deletBook();

    public Book searchById(int id) throws SQLException;

     void findAll() throws SQLException;

}
