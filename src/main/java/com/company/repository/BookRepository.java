package com.company.repository;

import com.company.model.Book;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class BookRepository implements BookDAO {

    private Book book;

    //creat book means insert book data to database
    @Override
    public void creatBook(int id,String title,String subject,int isbn,int status,int price,int penalty) throws SQLException {
        PreparedStatement statement = msCon.prepareStatement("INSERT INTO books"
                +"(id, title, subject, isbn, status, price, penalty)VALUES"+"(?,?,?,?,?,?,?);");
        statement.setInt(1,id);
        statement.setString(2,title);
        statement.setString(3,subject);
        statement.setInt(4,isbn);
        statement.setInt(5,status);
        statement.setInt(6,price);
        statement.setInt(7,penalty);
        statement.executeUpdate();

    }

    //read book means select book from data base
    @Override
    public ArrayList<Book> readBook() throws SQLException {
        PreparedStatement statement = msCon.prepareStatement("select title ,id,status,price,penalty from books");
        ResultSet resultSet = statement.executeQuery();
         ArrayList<Book> books = new ArrayList<>();

        while (resultSet.next()){
            Book book = new Book();
            book.setTitle(resultSet.getString("title"));
            book.setBookId(resultSet.getInt("id"));
            book.setStatus(resultSet.getInt("status"));
            book.setPrice(resultSet.getLong("price"));
            book.setPenalty(resultSet.getLong("penalty"));
          books.add(book);
        }
      return books;
    }
    //update book means insert new data to database
    @Override
    public void updateBook(int id,int status) throws SQLException {
       PreparedStatement statement = msCon.prepareStatement("update books set status = ? where id = ?;");
       statement.setInt(1,status);
       statement.setInt(2,id);
       statement.executeUpdate();

    }
    //delet book means delet data book from database
    @Override
    public void deletBook() {
        //TODO complet this method
    }

    @Override
    public Book searchById(int id) throws SQLException {
        PreparedStatement statement = msCon.prepareStatement("select * from books where id = ?");
        statement.setInt(1,id);
        ResultSet resultSet = statement.executeQuery();
        
        if (resultSet.next()){
            Book book = new Book();
            book.setTitle(resultSet.getString("title"));
            book.setSubject(resultSet.getString("subject"));
            book.setIsbn(resultSet.getLong("isbn"));
            book.setPenalty(resultSet.getInt("penalty"));
            book.setPrice(resultSet.getInt("price"));
            book.setStatus(resultSet.getInt("status"));

        }
        return book;
    }

    @Override
    public void findAll() throws SQLException {

        PreparedStatement statement = msCon.prepareStatement("select * from books;");
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()){
            String test = resultSet.getString("title");
            System.out.println(test);
        }
    }
}
