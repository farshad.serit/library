package com.company.repository;

import com.company.controller.MySQLCon;
import com.company.model.User;

import java.sql.Connection;
import java.sql.SQLException;

public interface UserDAO {
    MySQLCon obj = MySQLCon.getInstance("library");
    Connection msCon = obj.getConnection();

    //creat user means insert Specifications user into database
     public void creatUser(String fullName,String creatAt,int nationCode,int status,int budget) throws SQLException;

     //read user means get Specifications user from database
    public User readUser() throws SQLException;

    //update user means insert new data into database
    public void updateUser(long bodget,int id) throws SQLException;

    //delet user means delet data from database
    public void deletUser();

    public User sarchById(int id) throws SQLException;

    public void findAll() throws SQLException;

}
